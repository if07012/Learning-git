var express = require('express');
var router = express.Router();

var Generic = require('../genericService');
var app;
// create a new Generic called generic
var generic = new Generic();

var schemaGeneric = generic.createInstance('Schema', {
    name: { type: String, required: true, unique: true },
    content: { type: String, required: true },
    created_at: Date,
    updated_at: Date
}, app);
var userSchema = generic.createInstance('User', {
    userName: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    role: { type: String }
}, app);
userSchema.find({ userName: 'admin' }, function(err, items) {

    if (items.length === 0) {
        var model = new userSchema();
        model.userName = "admin";
        model.password = "admin";
        model.role = "-1";
        model.save(function(err, model) {
            if (err)
                console.log(err);
            console.log('User data created');
        });
    }

});

schemaGeneric.find({ name: 'Contact' }, function(err, items) {
    if (err)
        console.log(err);

    var strData;
    if (items.length === 0) {
        var str = JSON.stringify({
            name: { type: 'String', maxLength: 50, title: 'Full Name', inputType: 'textBox' },
            title: { type: 'String', maxLength: 15, title: 'Title', inputType: 'textBox' },
            description: { type: 'String', maxLength: 1000, title: 'Description', inputType: 'textArea' },
            email: { type: '[]', maxLength: 5, title: 'Email', inputType: 'listTextBox' },
            phone: { type: '[]', maxLength: 5, title: 'Phone Number', inputType: 'listTextBox' },
            address: { type: '[]', maxLength: 5, title: 'Address', inputType: 'listTextBox' },
            company: { type: '[]', maxLength: 5, title: 'Company', inputType: 'listTextBox' }
        });
        strData = str;
        var model = new schemaGeneric();
        model.name = 'Contact';
        model.content = str;
        model.created_at = new Date();
        model.save(function(err, model) {
            if (err)
                console.log(err);
            console.log('Schema Contact data created');
        });
    }
    if (items.length !== 0)
        strData = items[0].content;
    var obj = JSON.parse(strData);

    var contanct = generic.createInstance('Contact', strData, app);

    contanct.find(function(err, items) {
        if (err)
            res.send(err);
        if (items.length === 0) {
            console.log('create fake contact');
            for (var i = 0; i <= 20; i++) {
                var item = new contanct();
                item.description = faker.lorem.paragraph();
                item.name = faker.name.findName();
                item.title = item.name.indexOf('Miss') >= 0 || item.name.indexOf('Ms') >= 0 ? 'Female' : 'Male';
                item.email = [faker.internet.email()];
                item.phone = [faker.phone.phoneNumber()];
                item.address = [faker.address.streetAddress()];
                item.company = [faker.company.companyName()];
                item.save(function(err, model) {
                    if (err)
                        console.log(err);
                    console.log('data created');
                });
            }
        } else {
            console.log('data found');
        }


    });


});

function Api(app) {
    this.app = app;
    return {
        router: router
    }
}

module.exports = Api;