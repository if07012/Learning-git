var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var faker = require('faker');
var mongoose = require('mongoose');
var mongodb = require('mongodb');
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var authentication = require('./routes/authentication');
var api = require('./routes/api')(app);
var Generic = require('./genericService');
var generic = new Generic();
app.use('/Api', api.router);
app.use('/Authentication', authentication);



// var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/contacs');

// app.use(express.basicAuth('testUser', 'testPass'));


var listExpectedUrl = ["/"];
app.use(function(req, res, next) {
    next();
});


//for handle cors
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/bower_components'));

app.get('/:view', function(req, res) {
    res.render(req.params.view + '.jade', {});
});

app.get('/', function(req, res) {
    res.render('index.jade', {});
});
app.set('port', process.env.PORT || 3000);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user

app.use(function(err, req, res, next) {
    if (generic.get(req.url) !== undefined) {
        var data = generic.get(req.url);
        data.service(req, res, next);

        //res.send('hello');
    } else {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    }

});

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});